<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" >

    <title>Send Email | User</title>
</head>
<body>
<div class="container">
    <br><br><br>

    <h3 >Send Email To User</h3>
    <br>

    <form action="{{route('contact')}}" method="post">

        {{ csrf_token() }}

        <div class="form-group row">
            <label for="inputName" class="col-sm-2 col-form-label">Enter Name : </label>
            <div class="col-sm-8">
                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name ">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">Enter Email : </label>
            <div class="col-sm-8">
                <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email ">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputDescription" class="col-sm-2 col-form-label">Description : </label>
            <div class="col-sm-8">
                <textarea  class="form-control" rows="5" name="description" id="description" placeholder="Enter Email Body ....."></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputDescription" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-8">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>

</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
</body>
</html>